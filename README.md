#### Computation of P2 distance indicator

In this repository we showed the use of [p2distance](http://cran.r-project.org/web/packages/p2distance/) R package (Pérez-Luque et al. 2012). 

We showed a [tutorial](https://gitlab.com/iecolab/p2_distance/blob/master/tutorial_appendix.md) to compute the welfare of the andalusian municipalities in two periods. This exampe was included in the paper entitled ***Computation of synthetic indicator of welfare with open software. A study case for Andalusia (Spain)*** submitted to Social Indicator Research (Pérez-Luque et al.) 

#### More info at: 
* Antonio J. Pérez-Luque (1)
 * [@ajpelu](https://twitter.com/ajpelu)
 * [ajperez@ugr.es](mailto:ajperez@ugr.es)
 
* Ramón Pérez-Pérez (1)
 * [@rperezperez](https://twitter.com/@rperezperez)
 * [ramon@ugr.es](ramon@ugr.es)

(1) Laboratorio de Ecología (iEcolab), Instituto Interuniversitario Sistema Tierra (CEAMA), Universidad de Granada, Avda. del Mediterráneo s/n, Granada 18006, Spain.


License: Creative Commons Attribution 4.0 International License
![http://creativecommons.org/licenses/by/4.0/](https://i.creativecommons.org/l/by/4.0/88x31.png)

#### References 
Pérez-Luque, A.J., Moreno, R., Pérez-Pérez, R., & Bonet, F.J. (2012) p2distance: Well-being’s Synthetic Indicator. R package version 1.0.1. [http://CRAN.R-project.org/package=p2distance](http://CRAN.R-project.org/package=p2distance)

Pérez-Luque, A.J., Moreno, R., Pérez-Pérez, R., Bonet, F.J. & Somarriba, N. Computation of synthetic indicator of welfare with open software. A study case for Andalusia (Spain). Submitted to *Social Indicator Research*. 


