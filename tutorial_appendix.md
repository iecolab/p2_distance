This tutorial shows an example to use the p2distance pacakge. - Load the library [p2distance](http://cran.r-project.org/web/packages/p2distance/index.html)

``` r
# -----------------------------------------------------------
# Load packages
library('p2distance')
# -----------------------------------------------------------
```

-   Load the data. Available [here](http://www.iecolab.es/datasets/quality_andalusia.csv)

``` r
# -----------------------------------------------------------
# Read data from Url 
urlfile <- url('http://www.iecolab.es/datasets/quality_andalusia.csv')
mydata <- read.csv(urlfile, header=T, sep=',')
# -----------------------------------------------------------
```

-   Some exploration of the data

``` r
# -----------------------------------------------------------
# Explore the data
head(mydata)
```

    ##    cod year   entity migration births pop_growth restaurants bussines
    ## 1 4001 1989 19894001      2.70   9.96      -1.76        0.59    51.00
    ## 2 4001 2009 20094001      6.25   9.31      -3.99        2.66   100.40
    ## 3 4002 1989 19894002      2.33  11.67       3.89        0.00    47.96
    ## 4 4002 2009 20094002      2.61   2.24      -7.46        1.49    84.27
    ## 5 4003 1989 19894003      1.46  16.72       9.48        0.45    47.66
    ## 6 4003 2009 20094003      2.36  11.37       4.55        0.66    82.26
    ##   income phone  cars unemployed employed health teachers students schools
    ## 1   1086 17.12 16.24      15.28    24.75   0.59     7.62   135.40    0.59
    ## 2   4040 23.94 44.41       7.72    44.82   0.66     9.31    63.16    0.66
    ## 3    675 13.93 13.35       5.03    49.51   0.00     8.43   147.12    0.65
    ## 4   4005 21.70 45.26       6.48    48.24   0.00     8.20    72.33    0.75
    ## 5   1401 13.82 18.43       6.00    46.94   0.05     8.83   208.35    0.60
    ## 6   4972 18.97 40.37      14.42    57.99   0.04     7.47    98.67    0.29
    ##   libraries votes hostels inmigration   vans trucks buses age_index
    ## 1      0.59 57.39    0.00       26.38  49.82  21.10  2.93     76.15
    ## 2      0.00 63.03    0.66       27.26 135.64  45.21  0.00    210.00
    ## 3      0.65 59.49    0.00       26.57  75.18  14.26  1.30     69.97
    ## 4      0.75 68.23    0.00       38.03 256.52  80.54  1.49    255.38
    ## 5      0.05 41.64    0.10       10.83  59.44  27.70  2.60     24.86
    ## 6      0.12 51.94    0.08       18.91 173.06  74.02  0.66     70.42

``` r
names(mydata) # view variables names 
```

    ##  [1] "cod"         "year"        "entity"      "migration"   "births"     
    ##  [6] "pop_growth"  "restaurants" "bussines"    "income"      "phone"      
    ## [11] "cars"        "unemployed"  "employed"    "health"      "teachers"   
    ## [16] "students"    "schools"     "libraries"   "votes"       "hostels"    
    ## [21] "inmigration" "vans"        "trucks"      "buses"       "age_index"

``` r
# If we want to examine the values of the variable schools, we type
head(mydata["schools"])
```

    ##   schools
    ## 1    0.59
    ## 2    0.66
    ## 3    0.65
    ## 4    0.75
    ## 5    0.60
    ## 6    0.29

-   Prepare data
-   Change the effects of some variable. Variables with a negative relation to quality of life were reflected in the matrix of observations with a negative sign
-   Rename the row names. We will use the entity code (a identificador)
-   Select only the variables that we want to use to compute the synthetic indicator

``` r
# -----------------------------------------------------------
# Prepare data
# // 1  Change the effects of some variables 
mydata$migration <- mydata$migration * (-1)
mydata$unemployed <- mydata$unemployed * (-1)
mydata$age_index <- mydata$age_index * (-1)

# // 2 Rename rowname of this object with id (entity) of each municipality by year 
entities <- mydata[,3]
row.names(mydata) <- entities

# // 3  Select only the variables that we want to use to compute dp2 
myindicators <- mydata[,c(4:25)]
# -----------------------------------------------------------
```

-   Compute the reference base vector

``` r
# -----------------------------------------------------------
# Compute the reference base vector. We use the makeReferenceVector (see p2distnace documentation)
minRV <- makeReferenceVector(X=myindicators, reference_vector_function = min)
# -----------------------------------------------------------
```

-   Compute the P2 distance index

``` r
# -----------------------------------------------------------
# Compute the P2 Distance 
# We set a maximun of 50 iterations to achieve the convergence
ind <- p2distance(matriz=as.matrix(myindicators), reference_vector = minRV, iterations = 50)
```

    ## [1] "Iteration 1"
    ## [1] "Iteration 2"
    ## [1] "Iteration 3"

-   Explore results

``` r
# -----------------------------------------------------------
# Explore results and export 
# // see P2 distance index final
head(ind$p2distance)
```

    ##          p2distance.3
    ## 19894001        58.01
    ## 20094001        62.94
    ## 19894002        55.03
    ## 20094002        59.80
    ## 19894003        55.31
    ## 20094003        57.24

``` r
write.csv(ind$p2distance, file="out_p2distance.csv", row.names=TRUE)

# // How many interactions did need the algorithm to reach convergence? 
ind$iteration
```

    ## [1] 3

``` r
# // If we can see the differences between the P_2^j distances computed we typing
head(ind$diff_p2distances)
```

    ##          p2distance.1 p2distance.2 p2distance.3
    ## 19894001        24.43       0.6528            0
    ## 20094001        27.25       0.5999            0
    ## 19894002        25.72       0.8078            0
    ## 20094002        28.50       1.0340            0
    ## 19894003        27.34       0.6608            0
    ## 20094003        29.09       0.8627            0

``` r
# // See the different P_2^j distance index computed by each iteration
head(ind$diff_p2distance) 
```

    ##          p2distance.1 p2distance.2 p2distance.3
    ## 19894001        24.43       0.6528            0
    ## 20094001        27.25       0.5999            0
    ## 19894002        25.72       0.8078            0
    ## 20094002        28.50       1.0340            0
    ## 19894003        27.34       0.6608            0
    ## 20094003        29.09       0.8627            0

``` r
# // Explore the variables order, i.e., a vector with the variable names. 
# The order of variables names is the order of entrance determied by the last iteration 
ind$variables_sort
```

    ##  [1] "bussines"    "trucks"      "phone"       "cars"        "vans"       
    ##  [6] "income"      "employed"    "restaurants" "teachers"    "hostels"    
    ## [11] "inmigration" "libraries"   "votes"       "buses"       "health"     
    ## [16] "unemployed"  "migration"   "schools"     "students"    "age_index"  
    ## [21] "births"      "pop_growth"

``` r
write.csv(ind$variables_sort, file="out_order.csv", row.names=TRUE)

# // Correlation of each variable with sinthetic indicator 
ind$cor.coeff
```

    ##             p2distance.3
    ## migration      -0.192545
    ## births         -0.009174
    ## pop_growth     -0.008223
    ## restaurants     0.464903
    ## bussines        0.715043
    ## income          0.576474
    ## phone           0.625456
    ## cars            0.602957
    ## unemployed      0.240021
    ## employed        0.527077
    ## health          0.244958
    ## teachers        0.429622
    ## students        0.067246
    ## schools         0.170963
    ## libraries       0.333414
    ## votes           0.296317
    ## hostels         0.425773
    ## inmigration     0.404800
    ## vans            0.596792
    ## trucks          0.667304
    ## buses           0.263777
    ## age_index      -0.042441

``` r
write.csv(ind$cor.coeff, file="out_correlationCoeff.csv", row.names=TRUE)

# // Correction_factors 
ind$correction_factors
```

    ##    bussines      trucks       phone        cars        vans      income 
    ##      1.0000      0.6341      0.5816      0.4217      0.3097      0.3988 
    ##    employed restaurants    teachers     hostels inmigration   libraries 
    ##      0.6924      0.7406      0.9843      0.6416      0.7223      0.9077 
    ##       votes       buses      health  unemployed   migration     schools 
    ##      0.7536      0.9800      0.9691      0.8648      0.6354      0.5160 
    ##    students   age_index      births  pop_growth 
    ##      0.3419      0.6908      0.6763      0.2720

``` r
write.csv(ind$correction_factors, file="out_correctionFactors.csv", row.names=TRUE)

# // Discriminant coefficient 
ind$discrimination.coefficient
```

    ##   migration      births  pop_growth restaurants    bussines      income 
    ##      0.5916      0.4716      0.8466      1.3975      0.4878      0.8411 
    ##       phone        cars  unemployed    employed      health    teachers 
    ##      0.4954      0.5055      0.4933      0.3460      1.7596      0.6157 
    ##    students     schools   libraries       votes     hostels inmigration 
    ##      0.6098      0.9965      1.4302      0.1694      1.6698      0.7609 
    ##        vans      trucks       buses   age_index 
    ##      0.7064      0.7570      1.5807      0.8590

``` r
write.csv(ind$discrimination.coefficient, file="out_discriminantCoeff.csv", row.names=TRUE)
# -----------------------------------------------------------
```
